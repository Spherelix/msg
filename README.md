Das Projekt kann mit Visual Studio geöffnet werden.
Zum Ausführen des kompilierten Programmes muss lediglich die msg_standorte_deutschland.csv im gleichen Verzeichnis liegen.
Nach einiger Zeit (im Test unter 1min) gibt das Programm die benötigte Zeit, die optimale Tour und die Länge der optimalen Tour in Metern aus. Das Programm wurde lediglich auf Windows getestet, sollte aber auch unter Linux laufen.

Es wurde ein Branch-and-Bound Verfahren eingesetzt, da ja eine exakte Lösung gesucht wurde.
Das Verfahren ist einfach zu implementieren und schon mit einem relativ primitiven Algorithmus für die Berechnung der unteren Schranke und einer einfachen best-first Suchstrategie schnell genug, um das Problem in akzeptabler Zeit zu lösen.

Die optimale Tour ist:<br>
1 12 16 20 19 4 21 8 13 6 7 15 14 18 10 11 3 2 9 5 17 1 <br>
und hat eine Länge (die Entfernung zwischen den Standorten wurde auf Basis der Luftlinie zwischen den Geokoordinaten bestimmt) von etwa:<br>
2335426,8 m = 2335,4 km