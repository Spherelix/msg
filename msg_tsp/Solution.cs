﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace msg_tsp
{
    class Solution
    {
        static double[,] Distances;
        static double[] MinDistance;
        static double ShortestDistance = Double.MaxValue;

        List<int> tour;
        public List<int> UnvisitedOffices { get; private set; }
        public double Length { get; set; }

        private Solution(int count)
        {
            tour = new List<int>() { 0 };
            UnvisitedOffices = new List<int>();
            for (int i = 1; i < count; i++)
                UnvisitedOffices.Add(i);
            CalculateLength();
        }

        public Solution(Solution prevSolution, int visitNext)
        {
            tour = new List<int>(prevSolution.tour);
            tour.Add(visitNext);

            UnvisitedOffices = new List<int>(prevSolution.UnvisitedOffices);
            UnvisitedOffices.Remove(visitNext);

            CalculateLength();
        }

        public static Solution GetInitialSolution(double[,] distances, int count)
        {
            Distances = distances;
            MinDistance = new double[count];
            for (int i = 0; i < count; i++)
            {
                MinDistance[i] = Double.MaxValue;
                for (int j = 0; j < count; j++)
                {
                    if (j != i)
                    {
                        MinDistance[i] = Math.Min(MinDistance[i], distances[i, j]);
                        ShortestDistance = Math.Min(ShortestDistance, distances[i, j]);
                    }
                }
            }

            return new Solution(count);
        }

        public void CalculateLength()
        {
            Length = 0;
            for (int i = 1; i < tour.Count; i++)
            {
                Length += Distances[tour[i - 1], tour[i]];
            }
            Length += Distances[tour[tour.Count - 1], tour[0]];

            UnvisitedOffices.Sort((a, b) => Distances[tour[tour.Count - 1], a].CompareTo(Distances[tour[tour.Count - 1], b]));
        }

        public double GetLowerBound()
        {
            double minLength = Length - Distances[tour[tour.Count - 1], tour[0]];

            foreach (var index in UnvisitedOffices)
            {
                minLength += MinDistance[index];
            }
            minLength += ShortestDistance;

            return Math.Max(Length, minLength);
        }

        public override string ToString()
        {
            String str = "";
            foreach (var index in tour)
            {
                str += (index + 1) + " ";
            }
            str += "1\n";
            str += Length;
            return str;
        }
    }
}
